<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class FileType extends Enum
{
    public const DOCUMENT = 'document';
    public const TEMPLATE = 'template';
    public const BUCKET = 'bucket';
}
