<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class DocumentEntityType extends Enum
{
    public const COMPANY           = '01';
    public const PILOT             = '02';
    public const IMPORTED_CUSTOMER = '03';
}
