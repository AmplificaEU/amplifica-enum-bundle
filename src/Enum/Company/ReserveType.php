<?php

namespace Fusely\AmplificaEnumBundle\Enum\Company;

use MyCLabs\Enum\Enum;

class ReserveType extends Enum
{
    /** RII Reserve - contains balance amounts which come from payments of RII corrections */
    public const RII = 'RII Reserve';
    /** Payment Deposit - contains balance amounts which cannot be used yet */
    public const DEPOSIT = 'Payment Deposit';
    /** General Reserve - contains balance amounts which can be used */
    public const GENERAL = 'General Reserve';
    /** Tax Reserve - contains balance amounts which can are reserved for Tax and/or Operational Costs */
    public const TAX = 'Tax Reserve';
}
