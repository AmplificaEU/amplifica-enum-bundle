<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class ProgramPricingType extends Enum
{
    /**
     * Alphabetical order
     */

    public const GROUP = 'GROUP';
    public const LEARNER = 'LEARNER';
    public const LUMPSUM = 'LUMPSUM';
}
