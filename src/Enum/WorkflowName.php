<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class WorkflowName extends Enum
{
    /** entity-service */
    public const HANDSHAKE_STATUS        = 'handshake_status';
    public const ANSWER_STATUS           = 'answer_status';
    public const COMPANY_SEMAPHORE_STATE = 'company_semaphore_state';
    /** business-service */
    public const PROGRAM_LIFE_CYCLE               = 'program_life_cycle';
    public const PROGRAM_AUDIT                    = 'program_audit';
    public const INVOICEABLE_STATUS               = 'invoiceable_status';
    public const LEARNING_ACTIVITY_STATUS         = 'learning_activity_status';
    public const LEARNING_ACTIVITY_LEARNER_STATUS = 'learning_activity_learner_status';
    public const IMPORTED_INVOICE_STATE = 'imported_invoice_state';
}
