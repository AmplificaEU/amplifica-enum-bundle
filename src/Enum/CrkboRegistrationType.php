<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class CrkboRegistrationType extends Enum
{
    public const TEACHER     = 'TEACHER';
    public const INSTITUTION = 'INSTITUTION';
}
