<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class ArticleVatOrigin extends Enum
{
    public const AMPLIFICA = 'AmplificA AI+Domus';
    public const ORIGIN    = 'Origin';
}
