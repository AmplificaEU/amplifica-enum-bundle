<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class CompanySemaphoreState extends Enum
{
    public const GREY   = 'GREY';
    public const RED    = 'RED';
    public const ORANGE = 'ORANGE';
    public const GREEN  = 'GREEN';
}
