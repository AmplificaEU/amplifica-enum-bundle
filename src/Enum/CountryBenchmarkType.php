<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class CountryBenchmarkType extends Enum
{
    public const HOTEL_ROOMS = 'HOTEL_ROOMS';
    public const CONSUMPTION = 'CONSUMPTION';
}
