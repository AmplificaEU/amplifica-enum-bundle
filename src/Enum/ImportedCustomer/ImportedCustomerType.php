<?php

namespace Fusely\AmplificaEnumBundle\Enum\ImportedCustomer;

use MyCLabs\Enum\Enum;

class ImportedCustomerType extends Enum
{
    public const OVERRIDE      = 'OVERRIDE';
    public const DELETE        = 'DELETE';
    public const SKIP          = 'SKIP';
}
