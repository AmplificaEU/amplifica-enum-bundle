<?php

namespace Fusely\AmplificaEnumBundle\Enum\ImportedCustomer;

use MyCLabs\Enum\Enum;

class ImportedCustomerStatus extends Enum
{
    public const EXISTS = 'EXISTS';
    public const DIFFERENT_CONTACT = 'DIFFERENT_CONTACT';
    public const NOT_EXISTS = 'NOT_EXISTS';
    public const NOT_PROVIDED = 'NOT_PROVIDED';
}
