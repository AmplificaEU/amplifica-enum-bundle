<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class RelationshipType extends Enum
{
    public const CUSTOMER      = 'CUSTOMER';
    public const SUBCONTRACTOR = 'SUBCONTRACTOR';
    public const COMPANY       = 'COMPANY';
}
