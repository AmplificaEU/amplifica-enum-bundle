<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class CompanySemaphoreStateTransition extends Enum
{
    public const TO_RED    = 'to_red';
    public const TO_ORANGE = 'to_orange';
    public const TO_GREEN  = 'to_green';
}
