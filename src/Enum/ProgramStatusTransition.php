<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

/**
 * This class represents all the transition steps for a 'program_life_cycle' workflow
 *
 * @see config\packages\workflow.yaml
 */
class ProgramStatusTransition extends Enum
{
    public const TO_PROPOSED               = 'to_proposed';
    public const TO_PROPOSED_FROM_CUSTOMER = 'to_proposed_customer';
    public const TO_ACCEPTED               = 'to_accepted';
    public const TO_CANCELLED              = 'to_cancelled';
    public const TO_SCHEDULING             = 'to_scheduling';
    public const TO_RESCHEDULE             = 'to_reschedule';
    public const TO_PENDING                = 'to_pending';
    public const TO_STARTED                = 'to_started';
    public const TO_EXECUTED               = 'to_executed';
    public const TO_CUSTOMER_REJECTED      = 'to_customer_rejected';
    public const TO_INVOICED               = 'to_invoiced';
    public const TO_COMPLETED              = 'to_completed';
}
