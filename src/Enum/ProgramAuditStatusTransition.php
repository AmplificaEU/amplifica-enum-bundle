<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class ProgramAuditStatusTransition extends Enum
{
    public const TO_ACCEPTED = 'to_accepted';
    public const TO_REJECTED = 'to_rejected';
}
