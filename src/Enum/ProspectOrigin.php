<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class ProspectOrigin extends Enum
{
    public const MANUAL = 'MANUAL';
    public const WEBREGISTRATIONREQUEST = 'WEBREGISTRATIONREQUEST';
}
