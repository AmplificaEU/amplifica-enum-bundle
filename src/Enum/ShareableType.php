<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class ShareableType extends Enum
{
    public const INCOME = 'INCOME';
    public const PROFIT = 'PROFIT';
}
