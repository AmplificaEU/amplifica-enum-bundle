<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class AmplificaSettingType extends Enum
{
    public const COMPANY  = 'Company';
    public const USER     = 'User';
    public const PROTOCOL = 'Protocol';
    public const VALUE    = 'Value';
}
