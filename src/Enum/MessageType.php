<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class MessageType extends Enum
{
    public const CHAT = 'Chat';
    public const EMAIL = 'Email';
    public const NOTIFICATION = 'Notification';
}
