<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class UserRole extends Enum
{
    /** alphabetical order */

    public const ACCOUNTANT = 'ROLE_ACCOUNTANT';
    public const ADMIN      = 'ROLE_ADMIN';
    public const BAILIFF    = 'ROLE_BAILIFF';
    public const GOVERNMENT = 'ROLE_GOVERNMENT';
    public const PROVIDER   = 'ROLE_PROVIDER';
    public const TRAINER    = 'ROLE_TRAINER';
    public const TRANSLATOR = 'ROLE_TRANSLATOR';
    public const USER       = 'ROLE_USER';
}
