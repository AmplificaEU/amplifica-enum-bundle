<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class HandshakeStatusTransition extends Enum
{
    public const TO_APPROVED = 'to_approved';
    public const TO_REJECTED = 'to_rejected';
}
