<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class InvoiceableStatusTransition extends Enum
{
    public const TO_INVOICED = 'to_invoiced';
}
