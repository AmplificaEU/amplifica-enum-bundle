<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class AmplificaSettingEnum extends Enum
{
    public const PAS_COMPANY      = 'AmplificA Holding';
    public const PASAS_COMPANY    = 'AmplificA Shareholders';
    public const SPA_COMPANY      = 'AmplificA Program Counsil';
    public const ACVN_COMPANY     = 'AmplificA';
    public const SDA_COMPANY      = 'AprovA';
    public const PASPAL_COMPANY   = 'AmplificA Learner Association';
    public const BAILIFF_COMPANY  = 'Pan EU Bailiff';
    public const AI_DOMUS_COMPANY = 'AI+Domus';
    public const AI_DATA_COMPANY  = 'AI+Data';
    //public const AI_PERSON       = ''; // TODO Athena?
    public const ALLOW_USER_REGISTRATION       = 'Allow User Registration';
    public const GENERAL_VAT_EXEMPTED_PROGRAMS = 'General VAT Exempted Programs';
    public const GENERAL_VAT_LIABLE_PROGRAMS   = 'General VAT Liable Programs';
    public const RII_VAT_EXEMPTED_PROGRAMS     = 'RII VAT Exempted Programs';
    public const RII_VAT_LIABLE_PROGRAMS       = 'RII VAT Liable Programs';
    public const RII_LEARNING_MATERIAL         = 'RII Learning Material';
    public const WOW_LEARNERS                  = 'WOW+Learners';
    public const ALLOW_AMPLIFICA_ACCOUNTANT    = 'Allow AmplificA Accountant';
    public const AMPLIFICA_ACCOUNTANT_COMPANY  = 'AmplificA Accountant Company';
}
