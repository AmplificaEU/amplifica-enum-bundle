<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class LearningActivityType extends Enum
{
    /**
     * Alphabetical order
     */

    public const OFFLINE = 'OFFLINE';
    public const ONLINE = 'ONLINE';
}
