<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class CompanyActionsType extends Enum
{
    public const COMPANY_MUST_FILL_IA          = 'COMPANY_MUST_FILL_IA';
    public const COMPANY_MUST_FILL_ACCOUNTANT  = 'COMPANY_MUST_FILL_ACCOUNTANT';
}
