<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class ProspectType extends Enum
{
    public const CREATOR = "CREATOR+";
    public const BENEFICIARY = "BENEFICIARY";
    public const ACCOUNTANT = "ACCOUNTANT+";
    public const TEACHER = 'TEACHER';
}
