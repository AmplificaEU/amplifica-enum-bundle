<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class DocumentTemplateType extends Enum
{
    /** @deprecated */
    public const IVC = 'IVC';
    /** @deprecated */
    public const IVCP = 'IVCP';
    /** @deprecated */
    public const IVCL = 'IVCL';
    /** @deprecated */
    public const IVCU = 'IVCU';

    public const FEE      = 'Fee';
    public const INVOICE  = 'Invoice';
    public const SUMMA    = 'Collection Note';
    public const PROPOSAL = 'PROPOSAL';
}
