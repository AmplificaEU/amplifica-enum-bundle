<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class CompanyType extends Enum
{
    public const COMMERCIAL   = 'COMMERCIAL';
    public const GOVERNMENTAL = 'GOVERNMENTAL';
    public const PERSON       = 'PERSON';
    public const FISCAL_UNITY = 'FISCAL_UNITY';
    public const OTHER        = 'OTHER';
}
