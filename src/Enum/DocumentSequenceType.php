<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class DocumentSequenceType extends Enum
{
    public const INVOICE         = '01';
    public const PROPOSAL        = '02';
    public const PILOT           = '03';
    public const AUDIT           = '04';
    public const COLLECTION_NOTE = '05';
}
