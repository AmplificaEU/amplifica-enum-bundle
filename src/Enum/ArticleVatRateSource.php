<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class ArticleVatRateSource extends Enum
{
    public const COUNTRY = 'COUNTRY';
    public const EU      = 'EU';
}
