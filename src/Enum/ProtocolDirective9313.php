<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class ProtocolDirective9313 extends Enum
{
    public const OPTIN  = 'OPT-IN';
    public const OPTOUT = 'OPT-OUT'; // default
}
