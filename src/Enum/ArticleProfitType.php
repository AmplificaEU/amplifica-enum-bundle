<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class ArticleProfitType extends Enum
{
    public const SHARED_PROFIT = 'Shared Profit';
    public const AMPLIFICA_FEE = 'Invoiced Fee';
    public const NONE          = 'None';
}
