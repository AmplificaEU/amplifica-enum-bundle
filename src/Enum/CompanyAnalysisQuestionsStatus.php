<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class CompanyAnalysisQuestionsStatus extends Enum
{
    public const REJECTED = "REJECTED";
    public const PENDING = "PENDING";
    public const ACCEPTED = "ACCEPTED";
}
