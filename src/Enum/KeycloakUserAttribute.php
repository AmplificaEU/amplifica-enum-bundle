<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class KeycloakUserAttribute extends Enum
{
    public const COMPANY_ID = 'current_company';
    public const USER_ID = 'amp_id';
    public const USER_ROLE = 'current_role';
    public const GIVEN_NAME = 'given_name';
    public const FAMILY_NAME = 'family_name';
    public const EMAIL = 'email';
    public const PREFERRED_USERNAME = 'preferred_username';

    /** This attribute will define if token belongs to an impersonated account:
     * acr = 0 - impersonated account
     * acr = 1 - logged in account
     */
    public const AUTH_CONTEXT_REFERENCE = 'acr';
}
