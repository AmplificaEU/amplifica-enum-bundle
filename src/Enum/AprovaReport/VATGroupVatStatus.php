<?php

namespace Fusely\AmplificaEnumBundle\Enum\AprovaReport;

use MyCLabs\Enum\Enum;

class VATGroupVatStatus extends Enum
{
    public const LIABLE   = 'Liable';
    public const EXEMPTED = 'Exempted';
}
