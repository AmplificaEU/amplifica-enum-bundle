<?php

namespace Fusely\AmplificaEnumBundle\Enum\AprovaReport;

use MyCLabs\Enum\Enum;

class VATGroupType extends Enum
{
    public const UBO     = 'UBO';
    public const COMPANY = 'COMPANY';
}
