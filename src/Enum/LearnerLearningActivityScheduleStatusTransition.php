<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class LearnerLearningActivityScheduleStatusTransition extends Enum
{
    public const CHECKIN      = 'checkin';
    public const TO_CANCELLED = 'to_cancelled';
}
