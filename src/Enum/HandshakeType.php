<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class HandshakeType extends Enum
{
    public const ACCOUNTANT_SIGNUP_HANDSHAKE             = 'AccountantSignupHandshake';
    public const APROVA_REPORT_HANDSHAKE                 = 'AprovaReportHandshake';
    public const COMPANY_ANALYSIS_QUESTIONS_HANDSHAKE    = 'CompanyAnalysisQuestionsHandshake';
    public const COMPANY_JOIN_HANDSHAKE                  = 'CompanyJoinHandshake';
    public const IMPORTED_CUSTOMERS_HANDSHAKE            = 'ImportedCustomersHandshake';
    public const PROVIDER_SIGNUP_HANDSHAKE               = 'ProviderSignupHandshake';
    public const PRO_RATA_HANDSHAKE                      = 'ProRataHandshake';
    public const INVOICES_HANDSHAKE                      = 'InvoicesHandshake';
}
