<?php

namespace Fusely\AmplificaEnumBundle\Enum\Invoicing;

use MyCLabs\Enum\Enum;

class Metadata extends Enum
{
    /** invoicing party metadata */

    // used in invoice and collection note templates
    public const INVOICE_FROM_LOGO                = 'INVOICE_FROM_LOGO';
    public const INVOICE_FROM_FULL_NAME           = 'INVOICE_FROM_NAME';
    public const INVOICE_FROM_CHAMBER_OF_COMMERCE = 'INVOICE_FROM_CHAMBEROFCOMM';
    public const INVOICE_FROM_FISCAL_ID           = 'INVOICE_FROM_FISCALID';
    public const INVOICE_FROM_FULL_ADDRESS        = 'INVOICE_FROM_FULL_ADDRESS';

    /** party to invoice metadata */

    // used in invoice and collection note templates
    public const INVOICE_TO_FULL_NAME            = 'INVOICE_TO_NAME';
    // used in invoice and collection note templates
    public const INVOICE_TO_ADDRESS_STREET       = 'INVOICE_TO_ADDRESS_STREET';
    // used in invoice and collection note templates
    public const INVOICE_TO_ADDRESS_NUMBER       = 'INVOICE_TO_ADDRESS_NUMBER';
    // used in invoice and collection note templates
    public const INVOICE_TO_ADDRESS_ZIP          = 'INVOICE_TO_ADDRESS_ZIP';
    // used in invoice and collection note templates
    public const INVOICE_TO_ADDRESS_CITY         = 'INVOICE_TO_ADDRESS_CITY';
    // used in invoice and collection note templates
    public const INVOICE_TO_ADDRESS_STATE        = 'INVOICE_TO_ADDRESS_STATE';
    // used in invoice and collection note templates
    public const INVOICE_TO_ADDRESS_COUNTRY_NAME = 'INVOICE_TO_ADDRESS_COUNTRY';

    /** invoice metadata */

    // used in invoice and collection note templates
    public const INVOICE_DATE                          = 'INVOICE_DATE';
    public const INVOICE_NUMBER                        = 'INVOICE_NR';
    // used in invoice and collection note templates
    public const INVOICE_PURCHASE_ORDER_TAG            = 'TI_PO';
    // used in invoice and collection note templates
    public const INVOICE_PURCHASE_ORDER_NUMBER         = 'PO_NR';
    // used in invoice and collection note templates
    public const INVOICE_PROGRAM_NAME                  = 'PROGRAM_NAME';
    public const INVOICE_PROGRAM_PRICING_TYPE_TAG      = 'TI_PRICING_TYPE';
    public const INVOICE_PROGRAM_PRICING_TYPE          = 'PROGRAM_PRICING_TYPE';
    public const INVOICE_TEXT_DESCRIPTION              = 'TEXT_INVOICE_DESCRIPTION';
    public const INVOICE_TOTAL_VAT_AMOUNT              = 'VAT_TOTALAMOUNT';
    // used in invoice and collection note templates
    public const INVOICE_TOTAL_AMOUNT                  = 'TOTAL_AMOUNT';
    public const INVOICE_TEXT_DONOTPAY                 = 'TEXT_DONOTPAY';
    public const INVOICE_TEXT_DONOTPAY_ONLYBOOK        = 'TEXT_DONOTPAY_ONLYBOOK';
    public const INVOICE_TEXT_PROVIDER_THROUGH_CESSION = 'TEXT_PROVIDERTROUGHCESSION';
    // used in invoice and collection note templates
    public const INVOICE_PILOT_NUMBER                  = 'PILOT_NR';
    // used in invoice and collection note templates
    public const INVOICE_PROGRAM_CREATOR_NAME          = 'PROGRAM_CREATOR';

    /** invoice invoiceables metadata */

    public const INVOICEABLE_ARTICLE_TITLE          = 'I.ARTICLE_TITLE';
    public const INVOICEABLE_PROGRAM_MODULE_NAME    = 'I.PROGRAM_MODULE';
    public const INVOICEABLE_PROGRAM_ACTIVITY_TITLE = 'I.PROGRAM_ACTIVITY';
    public const INVOICEABLE_PROGRAM_START_DATE     = 'I.PROGRAM_EXECUTE_DATE';
    public const INVOICEABLE_VAT_RATE               = 'I.VAT_RATE';
    public const INVOICEABLE_VAT_AMOUNT             = 'I.VAT_AMOUNT';
    public const INVOICEABLE_NET_AMOUNT             = 'I.NET_AMOUNT';

    /** collection note metadata */

    public const COLLECTION_NOTE_NUMBER                            = 'SUMMA_NR';
    public const COLLECTION_NOTE_INVOICE_NUMBER                    = 'I.INVOICE_NUMBER';
    public const COLLECTION_NOTE_INVOICE_DATE                      = 'I.DATE';
    public const COLLECTION_NOTE_INVOICE_TOTAL_AMOUNT              = 'I.GROSS_AMOUNT';
    public const COLLECTION_NOTE_BAILIFF_COMPANY_NAME              = 'BAILIFFCOMPANY_NAME';
    public const COLLECTION_NOTE_BAILIFF_COMPANY_BANK_ACCOUNT_IBAN = 'BAILIFFCOMPANY_BANKACCOUNT';
    public const COLLECTION_NOTE_DUEDATE                           = 'DUEDATE';
}
