<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class ProgramPriceFrom extends Enum
{
    public const PER_PROGRAM = 'PER_PROGRAM';
    public const PER_MODULE  = 'PER_MODULE';
}
