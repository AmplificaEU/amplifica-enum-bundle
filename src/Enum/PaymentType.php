<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class PaymentType extends Enum
{
    public const INCOMING = 'INCOMING';
    public const OUTGOING = 'OUTGOING';
    public const DEPOSIT  = 'DEPOSIT';
    public const WITHDRAW = 'WITHDRAW';
}
