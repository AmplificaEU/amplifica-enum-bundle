<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class AmplificaEvents extends Enum
{
    // Handshake events
    public const HANDSHAKE_CREATED       = 'HANDSHAKE_CREATED';
    public const HANDSHAKE_APPROVED      = 'HANDSHAKE_APPROVED';
    public const HANDSHAKE_REJECTED      = 'HANDSHAKE_REJECTED';

    // Trigger events
    public const TRIGGER_HIT         = 'TRIGGER_HIT';
    public const TRIGGER_COMPLETED   = 'TRIGGER_COMPLETED';
    public const TRIGGER_FAILED      = 'TRIGGER_FAILED';
}
