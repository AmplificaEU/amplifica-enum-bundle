<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class TemplateType extends Enum
{
    public const VALIDATION = 'VALIDATION';
    public const RECOVERY   = 'RECOVERY';
}
