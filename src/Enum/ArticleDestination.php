<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class ArticleDestination extends Enum
{
    public const CUSTOMER = 'Customer';
    public const PROVIDER = 'Provider';
}
