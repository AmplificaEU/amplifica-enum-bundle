<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class CompanyStatus extends Enum
{
    public const ENABLED = "ENABLED";
    public const DISABLED = "DISABLED";
    public const PENDING = "PENDING";
}
