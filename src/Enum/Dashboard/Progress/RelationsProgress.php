<?php

namespace Fusely\AmplificaEnumBundle\Enum\Dashboard\Progress;

use MyCLabs\Enum\Enum;

class RelationsProgress extends Enum
{
    public const MISSING_RELATIONS = 'Please upload your Relations.';

    public const RELATIONS_REQUEST_VALIDATION = 'Request validation for your relations.';

    public const RELATIONS_ACCEPTED = 'Your relations were accepted.';

    public const RELATIONS_REJECTED = 'Your relations were rejected.';

    public const RELATIONS_PENDING = 'Your relations are waiting for approval.';
}
