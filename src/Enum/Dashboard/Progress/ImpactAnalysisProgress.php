<?php

namespace Fusely\AmplificaEnumBundle\Enum\Dashboard\Progress;

use MyCLabs\Enum\Enum;

class ImpactAnalysisProgress extends Enum
{
    public const MISSING_IMPACT_ANALYSIS = 'Please complete your impact analysis.';

    public const IMPACT_ANALYSIS_ACCEPTED = 'Your Impact Analysis is accepted.';

    public const IMPACT_ANALYSIS_REJECTED = 'Your impact analysis was rejected.';

    public const IMPACT_ANALYSIS_PENDING = 'Your impact analysis is waiting for approval.';
}
