<?php

namespace Fusely\AmplificaEnumBundle\Enum\Dashboard\Progress;

use MyCLabs\Enum\Enum;

class CompanyProgress extends Enum
{
    public const MISSING_COMPANY = 'Please fill in your Company,';

    public const MISSING_COMPANY_NAME = 'Please fill in your Company Name.';

    public const MISSING_COMPANY_ADDRESS = 'Please fill in your Company Address.';

    public const MISSING_COMPANY_BANK_ACCOUNT = 'Please fill in your Company Bank Account.';

    public const MISSING_COMPANY_SECTOR = 'Please fill in your Company Sector.';

    public const MISSING_COMPANY_PRIMARY_CONTACT = 'Please fill in your Primary Contact.';

    public const MISSING_COMPANY_ACCOUNTANT = 'Please fill in your Accountant.';

    public const COMPLETED = 'You completed your Company Profile';
}
