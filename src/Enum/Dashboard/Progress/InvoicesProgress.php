<?php

namespace Fusely\AmplificaEnumBundle\Enum\Dashboard\Progress;

use MyCLabs\Enum\Enum;

class InvoicesProgress extends Enum
{
    public const MISSING_INVOICES = 'Please upload your invoices.';

    public const INVOICES_REQUEST_VALIDATION = 'Request validation for your invoices.';

    public const INVOICES_ACCEPTED = 'Your invoices were accepted.';

    public const INVOICES_REJECTED = 'Your invoices were rejected.';

    public const INVOICES_PENDING = 'Your Invoices are waiting for approval…';

    public const INVOICES_UPLOAD = 'Your invoices are waiting for approval.';

    public const INVOICES_PROCESS = 'Process your invoices.';
}
