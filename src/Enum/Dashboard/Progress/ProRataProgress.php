<?php

namespace Fusely\AmplificaEnumBundle\Enum\Dashboard\Progress;

use MyCLabs\Enum\Enum;

class ProRataProgress extends Enum
{
    public const MISSING_PRO_RATA = 'Please complete your Impact Analysis - Annual Specifics Questionnaire.';

    public const PRO_RATA_ACCEPTED = 'Your Impact Analysis - Annual Specifics Questionnaire is accepted.';

    public const PRO_RATA_REJECTED = 'Your Impact Analysis - Annual Specifics Questionnaire was rejected.';

    public const PRO_RATA_PENDING = 'Your Impact Analysis - Annual Specifics Questionnaire is waiting for approval.';
}
