<?php

namespace Fusely\AmplificaEnumBundle\Enum\Dashboard\Progress;

use MyCLabs\Enum\Enum;

class UserProgress extends Enum
{
    public const MISSING_FIRST_NAME = 'Please fill in your First Name.';

    public const MISSING_LAST_NAME = 'Please fill in your Last Name.';

    public const MISSING_PHONE_NUMBER = 'Please fill in your Phone Number.';

    public const MISSING_ADDRESS = 'Please fill in your Address.';

    public const MISSING_LANGUAGE = 'Please fill in your Language.';

    public const COMPLETED = 'You\'re onboard';
}
