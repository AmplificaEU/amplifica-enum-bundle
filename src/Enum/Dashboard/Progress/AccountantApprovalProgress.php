<?php

namespace Fusely\AmplificaEnumBundle\Enum\Dashboard\Progress;

use MyCLabs\Enum\Enum;

class AccountantApprovalProgress extends Enum
{
    public const REQUEST_APPROVAL = 'Request approval for full access.';

    public const ACCOUNTANT_APPROVAL_ACCEPTED = 'All Set.';

    public const ACCOUNTANT_APPROVAL_PENDING = 'Waiting validation of company.';

    public const ACCOUNTANT_APPROVAL_REJECTED = 'Re-apply for approval for full access.';
}
