<?php

namespace Fusely\AmplificaEnumBundle\Enum\Dashboard\Progress;

use MyCLabs\Enum\Enum;

class CreatorApprovalProgress extends Enum
{
    public const MISSING_PRIMARY_ACCOUNTANT = 'Define a primary accountant.';

    public const REQUEST_APPROVAL = 'Request approval from your accountant.';

    public const CREATOR_APPROVAL_ACCEPTED = 'All Set.';

    public const CREATOR_APPROVAL_PENDING = 'Waiting validation from the accountant.';

    public const CREATOR_APPROVAL_REJECTED = 'Re-apply for approval for full access.';
}
