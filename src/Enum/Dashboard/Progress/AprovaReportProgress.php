<?php

namespace Fusely\AmplificaEnumBundle\Enum\Dashboard\Progress;

use MyCLabs\Enum\Enum;

class AprovaReportProgress extends Enum
{
    public const APROVA_REPORT_REQUEST_VALIDATION = 'Request validation for your aprova report.';

    public const APROVA_REPORT_ACCEPTED = 'Your Aprova Report was accepted.';

    public const APROVA_REPORT_REJECTED = 'Your Aprova Report was rejected.';

    public const APROVA_REPORT_PENDING = 'Your Aprova Report is waiting for approval.';
}
