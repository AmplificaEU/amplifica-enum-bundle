<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class InvoiceType extends Enum
{
    public const INVOICE = 'Invoice';
    public const FEE     = 'Fee';
    public const SUBCONTRACTOR = 'Subcontractor';
}
