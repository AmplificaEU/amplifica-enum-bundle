<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

/**
 * This class represents all the places (ie, Program::$status) for a 'program_life_cycle' workflow
 *
 * @see config\packages\workflow.yaml
 */
class ProgramStatus extends Enum
{
    public const DRAFTED           = 'DRAFTED';
    public const PROPOSED          = 'PROPOSED';
    public const ACCEPTED          = 'ACCEPTED';
    public const SCHEDULING        = 'SCHEDULING';
    public const PENDING           = 'PENDING';
    public const STARTED           = 'STARTED';
    public const EXECUTED          = 'EXECUTED';
    public const CUSTOMER_REJECTED = 'CUSTOMER_REJECTED';
    public const CANCELLED         = 'CANCELLED';
    public const INVOICED          = 'INVOICED';
    public const COMPLETED         = 'COMPLETED';
}
