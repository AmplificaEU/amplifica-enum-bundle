<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class LearningActivityScheduleStatus extends Enum
{
    public const PENDING   = 'PENDING';
    public const EXECUTED  = 'EXECUTED';
    public const CANCELLED = 'CANCELLED';
}
