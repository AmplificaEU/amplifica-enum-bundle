<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class TeacherType extends Enum
{
    public const TEACHER       = 'TEACHER';
    public const SUBCONTRACTOR = 'SUBCONTRACTOR';
}
