<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class LearningActivityScheduleStatusTransition extends Enum
{
    public const TO_EXECUTED  = 'to_executed';
    public const TO_CANCELLED = 'to_cancelled';
}
