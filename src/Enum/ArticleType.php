<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class ArticleType extends Enum
{
    public const PRODUCT     = 'PRODUCT';
    public const SERVICE     = 'SERVICE';
    public const SPLIT       = 'SPLIT';
    public const DOWNPAYMENT = 'DOWNPAYMENT';
}
