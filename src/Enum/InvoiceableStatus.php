<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class InvoiceableStatus extends Enum
{
    public const PENDING  = 'PENDING';
    public const INVOICED = 'INVOICED';
    public const REFUNDED = 'REFUNDED';
}
