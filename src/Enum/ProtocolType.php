<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class ProtocolType extends Enum
{
    public const AMPLIFICA_CERTIFIED_SOLUTION = 'AmplificA Certified Solution';
    public const OPEN_EDUCATION = 'Open Education';
    public const RII = 'RII';
}
