<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class ArticleOrigin extends Enum
{
    public const APROVA    = AmplificaSettingEnum::SDA_COMPANY;
    public const AMPLIFICA = AmplificaSettingEnum::ACVN_COMPANY;
    public const PROVIDER  = 'Provider through Cession';
}
