<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class VatType extends Enum
{
    public const HIGH   = 'HIGH';
    public const LOW    = 'LOW';
    public const LOWLOW = 'LOWLOW';
    public const EXEMPT = 'EXEMPT';
}
