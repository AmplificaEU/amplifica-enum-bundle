<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class ShareholderType extends Enum
{
    // shareholder owner company is type PERSON
    public const UBO = 'UBO';
    // shareholder owner company is not type PERSON
    public const COMPANY = 'COMPANY';
}
