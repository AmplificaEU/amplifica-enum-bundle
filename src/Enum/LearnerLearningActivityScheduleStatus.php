<?php

namespace Fusely\AmplificaEnumBundle\Enum;

use MyCLabs\Enum\Enum;

class LearnerLearningActivityScheduleStatus extends Enum
{
    public const INVITED   = 'INVITED';
    public const ATTENDED  = 'ATTENDED';
    public const CANCELLED = 'CANCELLED';
}
