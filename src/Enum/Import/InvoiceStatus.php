<?php

namespace Fusely\AmplificaEnumBundle\Enum\Import;

use MyCLabs\Enum\Enum;

class InvoiceStatus extends Enum
{
    /** Default status - default */
    public const PENDING = 'PENDING';
    /** Invalid original invoice not ready for process - set after import */
    public const INVALID = 'INVALID';
    /** Valid original invoice ready for process - set after import */
    public const VALID = 'VALID';
    /** Valid invoices processed - set after invoices processed */
    public const PROCESSED = 'PROCESSED';
    /** RII calculations done - ready to be re-invoiced - set after aprova report handshake */
    public const DONE = 'DONE';
    /** Started re-invoicing process */
    public const INVOICING = 'INVOICING';
    /** Ended re-invoicing process */
    public const INVOICED = 'INVOICED';
    /** Started paying process - transferring correction amounts to beneficiaries reserves */
    public const PAYING = 'PAYING';
    /** Ended paying process - transferred correction amounts to beneficiaries reserves */
    public const PAID = 'PAID';
    /**
     * All imported invoices that have been paid or do not require RII will be in this status.
     * @TODO: check if Imported invoices that have been paid, require some handshake to set this status.
     *
     * Final status
     */
    public const COMPLETED = 'COMPLETED';
}
