<?php

namespace Fusely\AmplificaEnumBundle\Enum\Import\InvoiceAnalysis;

use MyCLabs\Enum\Enum;

class PermissionReason extends Enum
{
    public const PASS = 'Non-CRKBO-Institution sent VAT Liable Invoice';
    public const PASS_CRKBO_TEACHER_SENT_EDUCATION =
        'CRKBO-Teacher sent VAT Exempted Invoice to education Beneficiary+';
    public const FAIL_NON_CRKBO_SENT_EXEMPTED = 'Non-CRKBO-Institution sent VAT Exempted Invoice';
    public const FAIL_CRKBO_INSTITUTION_SENT_EXEMPTED = 'CRKBO-Institution sent VAT Exempted Invoice';
    public const FAIL_CRKBO_INSTITUTION_SENT_LIABLE = 'CRKBO-Institution sent VAT Liable Invoice';
    public const FAIL_CRKBO_TEACHER_SENT_NON_EDUCATION = 'CRKBO-Teacher sent Invoice to non-education Beneficiary+';
    public const FAIL_CRKBO_TEACHER_SENT_EDUCATION = 'CRKBO-Teacher sent VAT Liable Invoice to education Beneficiary+';
    public const FAIL = 'Cannot check if Invoice is VAT Liable or Exempted';
}
