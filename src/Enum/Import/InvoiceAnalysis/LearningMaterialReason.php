<?php

namespace Fusely\AmplificaEnumBundle\Enum\Import\InvoiceAnalysis;

use MyCLabs\Enum\Enum;

class LearningMaterialReason extends Enum
{
    public const PASS = 'Learning Material Provided';
    public const WARNING = 'Learning Material less than 5%';
}
