<?php

namespace Fusely\AmplificaEnumBundle\Enum\Import\InvoiceAnalysis;

use MyCLabs\Enum\Enum;

class AmplificaFeeReason extends Enum
{
    public const APPLIED     = 'Applied';
    public const NOT_APPLIED = 'Not applied';
}
