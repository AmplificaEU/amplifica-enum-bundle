<?php

namespace Fusely\AmplificaEnumBundle\Enum\Import\InvoiceAnalysis;

use MyCLabs\Enum\Enum;

class PracticaVATReason extends Enum
{
    public const CHARGED = 'VAT charged on VAT Exempted Beneficiary';
}
