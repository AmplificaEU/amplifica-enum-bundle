<?php

namespace Fusely\AmplificaEnumBundle\Enum\Import\InvoiceAnalysis;

use MyCLabs\Enum\Enum;

class Status extends Enum
{
    /** ✅ */
    public const OK = 'OK';
    /** ⚠️ */
    public const WARNING = 'WARNING';
    /** 🕒 */
    public const PENDING = 'PENDING';
    /** ⛔️ */
    public const ERROR = 'ERROR';
    /** ⏩ */
    public const SKIP = 'SKIP';
}
