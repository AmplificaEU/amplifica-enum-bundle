<?php

namespace Fusely\AmplificaEnumBundle\Enum\Import\InvoiceAnalysis;

use MyCLabs\Enum\Enum;

class SurchargeReason extends Enum
{
    public const PASS       = 'Surcharge less than 4%';
    public const FAIL_OVER  = 'Surcharge more than 4%, but not exactly 10%';
    public const FAIL_EXACT = 'Surcharge is exactly 10%';
}
