<?php

namespace Fusely\AmplificaEnumBundle\Enum\Import\InvoiceAnalysis;

use MyCLabs\Enum\Enum;

class Correction9313Reason extends Enum
{
    // Beneficiary is not a WOW learner
    public const PASS    = 'Not applicable';
    // Beneficiary is WOW learner but the program was marked as not cancelled
    public const NO_DATA = 'No data provided';
    // Beneficiary is a WOW learner and the program was marked as cancelled
    public const FAIL    = 'Fail';
    // Invoice merged with credit note
    public const CREDITED = 'Skipped because of creditnote';
}
