<?php

namespace Fusely\AmplificaEnumBundle\Enum\Import\InvoiceAnalysis;

use MyCLabs\Enum\Enum;

class VATReason extends Enum
{
    public const PASS = 'Pass';
    public const FAIL = 'Fail';
    public const SKIPPED = 'Skipped';
    public const WARNING = 'No VAT Applied on Non Educational Services/Products';
}
