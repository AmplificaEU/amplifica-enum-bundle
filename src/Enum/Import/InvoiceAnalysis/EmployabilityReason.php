<?php

namespace Fusely\AmplificaEnumBundle\Enum\Import\InvoiceAnalysis;

use MyCLabs\Enum\Enum;

class EmployabilityReason extends Enum
{
    public const PASS                                  = 'VAT Liable Invoice to VAT Liable Beneficiary+';
    public const FAIL_LIABLE_TO_WOW_LEARNER            = 'VAT Liable Invoice to WOW+Learner';
    public const FAIL_VAT_LIABLE_TO_EXEMPT_BENEFICIARY = 'VAT Liable Invoice to VAT Exempted Beneficiary+';
    public const FAIL_EXEMPTED_TO_WOW_LEARNER          = 'VAT Exempted Invoice to WOW+Learner';
    public const FAIL_EXEMPTED_TO_EXEMPTED_BENEFICIARY = 'VAT Exempted Invoice to VAT Exempted Beneficiary+';
    public const FAIL_EXEMPTED_TO_LIABLE_BENEFICIARY   = 'VAT Exempted Invoice to VAT Liable Beneficiary+';
    public const FAIL = 'Cannot check if Invoice is VAT Liable or Exempted';
}
