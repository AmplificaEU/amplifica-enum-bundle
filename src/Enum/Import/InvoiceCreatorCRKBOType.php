<?php

namespace Fusely\AmplificaEnumBundle\Enum\Import;

use Fusely\AmplificaEnumBundle\Enum\CrkboRegistrationType;

class InvoiceCreatorCRKBOType extends CrkboRegistrationType
{
    public const NONE = 'NONE';
}
