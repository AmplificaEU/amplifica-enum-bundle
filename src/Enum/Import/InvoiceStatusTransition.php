<?php

namespace Fusely\AmplificaEnumBundle\Enum\Import;

use MyCLabs\Enum\Enum;

class InvoiceStatusTransition extends Enum
{
    public const TO_INVALID = 'to_invalid';
    public const TO_VALID = 'to_valid';
    public const TO_PROCESSED = 'to_processed';
    public const TO_DONE = 'to_done';
    public const TO_INVOICING = 'to_invoicing';
    public const TO_INVOICED = 'to_invoiced';
    public const TO_PAYING = 'to_paying';
    public const TO_PAID = 'to_paid';
    public const TO_COMPLETED = 'to_completed';
}
