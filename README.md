# AmplificA Enum Bundle
Includes all enums used by AmplificA services/projects which can be shared.

# Usage

## Add repository to composer.json

```
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/AmplificaEU/amplifica-enum-bundle.git"
        }
    ]
```

## Run composer require
```
$ composer require "fusely/amplifica-enum-bundle"
```
